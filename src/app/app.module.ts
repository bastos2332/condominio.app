import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy, NavParams } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { ComponentModule } from './componentes/component/component.module';
import { TabsPageModule } from './tabs/tabs.module';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { VerticalTimelineModule } from 'angular-vertical-timeline';
import { HttpErrorInterceptor } from './util/http-error-interceptor';
import { Clipboard } from '@ionic-native/clipboard/ngx';

// import { File } from '@ionic-native/file/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    ComponentModule,
    TabsPageModule,
    VerticalTimelineModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SocialSharing,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
    Clipboard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
