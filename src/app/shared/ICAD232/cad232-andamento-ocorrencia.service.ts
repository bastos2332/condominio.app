import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ICAD232AndamentoOcorrencia } from './icad232-andamento-ocorrencia';

@Injectable({
  providedIn: 'root'
})
export class CAD232AndamentoOcorrenciaService {

  constructor(private http: HttpClient) { }


  get(idOCoorencia: number) {
     return this.http.get<ICAD232AndamentoOcorrencia[]>(environment.apiUrl + '/CAD232/' + idOCoorencia).toPromise();
  }

}
