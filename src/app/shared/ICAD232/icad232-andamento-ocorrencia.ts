export interface ICAD232AndamentoOcorrencia {
    IdAndamentoOcorrencia: number;
    IdSolicitacaoOcorrencia: number;
    IdUsuarioAndamento: number;
    DescricaoAndamento: string;
    DataAndamento: string;
    IdStatusOcorrencia: number;
    IdCliente: number;
    Imagem: string;
}
