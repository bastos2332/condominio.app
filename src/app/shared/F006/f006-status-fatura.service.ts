import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IF006StatusFatura } from './if006-status-fatura';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class F006StatusFaturaService {

  constructor(private http: HttpClient) { }

  Get() {
    return this.http.get<IF006StatusFatura[]>(environment.apiUrl + '/F006').toPromise();
  }
}
