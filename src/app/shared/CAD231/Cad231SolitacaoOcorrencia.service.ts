/* #region IMPORTS*/
import { Injectable } from '@angular/core';
import { } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { ICAD231SolitacaoOcorrencia } from './ICAD231SolicitacaoOcorrencia';
import { environment } from 'src/environments/environment';
/* #endregion */

@Injectable({
  providedIn: 'root'
})
export class CAD231SolicitacaoOcorrenciaService {
  ocorrencia: ICAD231SolitacaoOcorrencia;
  ocorrencias: ICAD231SolitacaoOcorrencia[];

  constructor(private http: HttpClient) { }

  getCAD200(idCliente: number) {

    return this.http.get<ICAD231SolitacaoOcorrencia[]>(environment.apiUrl + '/CAD231?idCliente=' + idCliente).toPromise();

  }

  Post(ocorrencia: ICAD231SolitacaoOcorrencia) {

    const head = {
      headers: {
        'Content-Type': 'application/json'
      }
    };

    return this.http.post(environment.apiUrl + '/CAD231', ocorrencia, head).toPromise();

  }

  get(idOCorrencia: number) {
   return this.http.get<ICAD231SolitacaoOcorrencia>(environment.apiUrl + '/CAD231?idOcorrencia=' + idOCorrencia).toPromise();
  }

  Put(id: number, ocorrencia: ICAD231SolitacaoOcorrencia) {

  }



}
