export interface ICAD231SolitacaoOcorrencia {
    DataCadastroSolicitacao: string;
    DescricaoSolicitacao: string;
    IdCliente: number;
    IdProvedor: number;
    IdSolicitacaoOCorrencia: number;
    IdStatusOcorrencia: number;
    IdTipoSolicitacaoOcorrencia: number;
    IdUsuarioCadastro: number;
}

