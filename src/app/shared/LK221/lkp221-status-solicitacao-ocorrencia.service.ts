import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ILKP221StatusSolicitacaoOcorrencia } from './Ilkp221-status-solicitacao-ocorrencia';
@Injectable({
  providedIn: 'root'
})
export class LKP221StatusSolicitacaoOcorrenciaService {

  constructor(private http: HttpClient) { }


  get() {
    return this.http.get<ILKP221StatusSolicitacaoOcorrencia[]>(environment.apiUrl + '/LKP221').toPromise();
  }
}
