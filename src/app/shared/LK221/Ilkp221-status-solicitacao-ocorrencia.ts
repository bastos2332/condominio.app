export interface ILKP221StatusSolicitacaoOcorrencia {
    IdStatusOcorrencia: number;
    IdProvedor: number;
    StatusOcorrencia: string;
    TipoAcao: string;
}
