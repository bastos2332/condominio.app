import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  private isLoading = false;

  constructor(private loadingController: LoadingController) { }

  async Carregando() {
    this.isLoading = true;
    return await this.loadingController.create({
      duration: 20000,
    }).then(a => {
      a.present().then(() => {
        if (!this.isLoading) {
          a.dismiss().then();
        }
      });
    });
  }

  async FecharCarregando() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then();
  }
}
