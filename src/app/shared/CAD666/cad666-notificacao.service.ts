import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICAD666Notificacao } from './icad666-notificacao';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CAD666NotificacaoService {

  constructor(private http: HttpClient) { }

  get(idCliente: number) {
    return this.http.get<ICAD666Notificacao[]>
    (environment.apiUrl + '/CAD666?idCliente=' + idCliente)
    .toPromise();
  }

  VisualizarAlertaNotificacao(idCliente: number) {
    const head = {
      headers: {
          'Content-Type': 'application/json'
      }
  };
    this.http.put(environment.apiUrl + '/CAD666', idCliente, head).toPromise();
  }

  VisualizarConteudoNotificacao(idNotificacao: number) {
    const head = {
      headers: {
          'Content-Type': 'application/json'
      }
  };
    this.http.put(environment.apiUrl + '/CAD666/VIEW-CONTEUDO', idNotificacao, head).toPromise();
  }
}
