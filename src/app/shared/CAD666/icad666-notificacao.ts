export interface ICAD666Notificacao {
    IdNotificacao: number;
    IdCliente: number;
    IdProvedor: number;
    TextoNotificacao: string;
    DataNotificacao: string;
    RotaNotificacao: string;
    IsVisualizada: boolean;
    IsConteudoVisualizado: boolean;
}
