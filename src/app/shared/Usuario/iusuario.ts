export interface IUsuario {
    IdUsuario: number;
    NomeUsuario: string;
    LoginUsuario: string;
    SenhaUsuario: string;
    Cpf: string;
    IdPerfil: number;
    IdStatusUsuario: number;
    IsBloqueadoSistema: number;
    TipoAcesso: string;
    IdProvedor: number;
    IdCliente: number;
    EmailUsuario: string;
    nomeArquivoFoto: string;
}

