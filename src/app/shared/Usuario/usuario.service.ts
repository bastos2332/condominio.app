import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IUsuario } from './iusuario';
import { environment } from 'src/environments/environment';
import { IUsuarioAuth } from './iusuario-auth';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http: HttpClient) { }

  Logar(usuario: IUsuarioAuth) {
    const head = {
      headers: {
          'Content-Type': 'application/json'
      }
  };
    return this.http.post<IUsuario>(environment.apiUrl + '/Usuario' , usuario, head).toPromise();
  }
}
