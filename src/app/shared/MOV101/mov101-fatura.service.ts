import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IMOV101Fatura } from './imov101-fatura';

@Injectable({
  providedIn: 'root'
})
export class MOV101FaturaService {

  constructor(private http: HttpClient) { }

  GetFaturas(idProvedor: number, idCliente: number, isProvedorMaster: boolean) {

    return this.http.get<IMOV101Fatura[]>
      (
        `${environment.apiUrl}/MOV101?idProvedor=${idProvedor}&idCliente=${idCliente}&isProvedorMaster=${isProvedorMaster}`
      )
      .toPromise();
  }
}
