export interface IMOV101Fatura {
    IdFatura: number;
    DataVencimento: string;
    ValorTotal: number;
    IdStatusFatura: number;
    IdCliente: number;
    IdProvedor: number;
    ChaveBoletoWeb: string;
}
