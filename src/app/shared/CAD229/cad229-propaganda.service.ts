import { Injectable } from '@angular/core';
import { ICAD229Propaganda } from './ICAD229-propaganda';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CAD229PropagandaService {

  constructor(private http: HttpClient) { }

  getList(idCondominio: number){
    return this.http.get<ICAD229Propaganda[]>(environment.apiUrl + '/CAD229?idProvedor=' + idCondominio).toPromise();
  }

}
