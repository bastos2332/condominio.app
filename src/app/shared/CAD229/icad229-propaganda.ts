export interface ICAD229Propaganda {
    IdPropaganda: number;
    IdProvedor: number;
    IdUsuario: number;
    TituloPropaganda: string;
    NomeArquivo: string;
    DescricaoPropaganda: string;
}
