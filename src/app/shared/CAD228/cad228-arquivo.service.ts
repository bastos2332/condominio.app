import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ICAD228Arquivo } from './icad228-arquivo';

@Injectable({
  providedIn: 'root'
})
export class CAD228ArquivoService {
  
  constructor(private http: HttpClient) { }

  get(idProvedor: number) {
    return this.http.get<ICAD228Arquivo[]>(environment.apiUrl + '/CAD228?idProvedor=' + idProvedor).toPromise();
  }
}
