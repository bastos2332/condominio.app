export interface ICAD228Arquivo {
    IdArquivo: number;
    IdSolicitacaoOcorrencia: number;
    IdAndamentoOcorrencia: number;
    NomeArquivo: string;
    Descricao: string;
    Tamanho: string;
    Extensao: string;
    DataUpload: string;
    CaminhoArquivo: string;
    IdCLiente: number;
    IdUsuario: number;
}
