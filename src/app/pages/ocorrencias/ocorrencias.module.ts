import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { OcorrenciasPage } from './ocorrencias.page';
import { UsuarioService } from 'src/app/shared/Usuario/usuario.service';
import { CAD231SolicitacaoOcorrenciaService } from 'src/app/shared/CAD231/Cad231SolitacaoOcorrencia.service';
import { ComponentModule } from 'src/app/componentes/component/component.module';
import { AndamentoOcorrenciaPageModule } from '../andamento-ocorrencia/andamento-ocorrencia.module';

const routes: Routes = [
  {
    path: '',
    component: OcorrenciasPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AndamentoOcorrenciaPageModule,
    RouterModule.forChild([{ path: '', component: OcorrenciasPage }])
  ],
  declarations: [OcorrenciasPage],
  providers: [UsuarioService, CAD231SolicitacaoOcorrenciaService],
  exports: [OcorrenciasPage]
})
export class OcorrenciasPageModule {}
