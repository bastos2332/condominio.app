import { Component, OnInit } from '@angular/core';
import { CAD231SolicitacaoOcorrenciaService } from 'src/app/shared/CAD231/Cad231SolitacaoOcorrencia.service';
import { Router } from '@angular/router';
import { ICAD231SolitacaoOcorrencia } from 'src/app/shared/CAD231/ICAD231SolicitacaoOcorrencia';
import { IUsuario } from 'src/app/shared/Usuario/iusuario';
import { Storage } from '@ionic/storage';
import { LoadingService } from 'src/app/shared/Loading/loading.service';
import { LKP221StatusSolicitacaoOcorrenciaService } from 'src/app/shared/LK221/lkp221-status-solicitacao-ocorrencia.service';
import { ILKP221StatusSolicitacaoOcorrencia } from 'src/app/shared/LK221/Ilkp221-status-solicitacao-ocorrencia';
import { ModalController, NavController } from '@ionic/angular';
import { AndamentoOcorrenciaPage } from '../andamento-ocorrencia/andamento-ocorrencia.page';
@Component({
  selector: 'app-ocorrencias',
  templateUrl: './ocorrencias.page.html',
  styleUrls: ['./ocorrencias.page.scss']
})
export class OcorrenciasPage implements OnInit {

  public UsuarioLogado: IUsuario;
  public CAD231OcorrenciasList: ICAD231SolitacaoOcorrencia[];
  public LKP221StatusLista: ILKP221StatusSolicitacaoOcorrencia[] = [];

  constructor(
    private ocorrenciaService: CAD231SolicitacaoOcorrenciaService,
    private statusService: LKP221StatusSolicitacaoOcorrenciaService,
    private route: Router,
    private storage: Storage,
    private loading: LoadingService,
    public navCtrl: NavController,
    private modalCtrl: ModalController
  ) { }

  iconIsForWard = true;

  ngOnInit() {
    this.loading.Carregando();
    this.CarregarUsuario();
  }

  CarregarUsuario() {
    this.storage.get('usuario').then(res => {
      this.UsuarioLogado = res;
      this.CarregarOcorrencias();
    });
  }

  CarregarOcorrencias() {
    this.ocorrenciaService.getCAD200(this.UsuarioLogado.IdCliente).then(res => {
    this.CAD231OcorrenciasList = res;
    this.CarregarStatus();
    });
  }
  CarregarStatus() {
    this.statusService.get().then((res) => {
      const escopo = this;
      res.map(function(elemento, index) {
        if (escopo.CAD231OcorrenciasList.find(ocorrencia => ocorrencia.IdStatusOcorrencia === elemento.IdStatusOcorrencia)) {
          escopo.LKP221StatusLista.push(elemento);
        }
      });
    });
    this.loading.FecharCarregando();
  }



  getStatusOcorrencia(idStaus: number) {
    for (let i = 0; i < this.LKP221StatusLista.length; i++) {
      if (this.LKP221StatusLista[i].IdStatusOcorrencia === idStaus) {
        return this.LKP221StatusLista[i].StatusOcorrencia;
      }
    }
  }

  goForm(idOcorrencia?: number) {
    this.route.navigateByUrl('/tabs/ocorrencias/form/' + idOcorrencia);
  }

  async AbrirAndamentos(idOcorrencia: number) {
    console.log(idOcorrencia);
    const modal = await this.modalCtrl.create({
      component: AndamentoOcorrenciaPage,
      componentProps: { 'value': idOcorrencia },
      animated: true,
      keyboardClose: true,
      showBackdrop: true
    });
    return await modal.present();
  }


  AlterarIcoAcaoLateral() {
    this.iconIsForWard = !this.iconIsForWard;
  }
}
