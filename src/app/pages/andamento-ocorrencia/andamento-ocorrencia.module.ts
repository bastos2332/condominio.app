import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { AndamentoOcorrenciaPage } from './andamento-ocorrencia.page';
import { VerticalTimelineModule } from 'angular-vertical-timeline';


const routes: Routes = [
  {
    path: '',
    component: AndamentoOcorrenciaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    VerticalTimelineModule
  ],
  declarations: [AndamentoOcorrenciaPage]
})
export class AndamentoOcorrenciaPageModule {}
