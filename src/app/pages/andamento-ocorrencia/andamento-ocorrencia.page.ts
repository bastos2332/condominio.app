import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { LoadingService } from 'src/app/shared/Loading/loading.service';
import { CAD232AndamentoOcorrenciaService } from 'src/app/shared/ICAD232/cad232-andamento-ocorrencia.service';
import { ICAD232AndamentoOcorrencia } from 'src/app/shared/ICAD232/icad232-andamento-ocorrencia';
@Component({
  selector: 'app-andamento-ocorrencia',
  templateUrl: './andamento-ocorrencia.page.html',
  styleUrls: ['./andamento-ocorrencia.page.scss'],
})
export class AndamentoOcorrenciaPage implements OnInit {
  public CAD232AndamentoList: ICAD232AndamentoOcorrencia[];

  events = [{
    badgeClass: 'info',
    badgeIconClass: 'glyphicon-check',
    title: 'First heading',
    content: 'Some awesome content.'
  }, {
    badgeClass: 'warning',
    badgeIconClass: 'glyphicon-credit-card',
    title: 'Second heading',
    content: 'More awesome content.'
  }];

  constructor(
    private modalCtrl: ModalController,
    private loading: LoadingService,
    private CAD232Service: CAD232AndamentoOcorrenciaService
     ) { }

  ngOnInit() {
    
    this.loading.Carregando();
    this.CarregarAndamentos(38);
    console.log('Chamou este cara');
  }

  FecharModal() {
    this.modalCtrl.dismiss();
  }

  CarregarAndamentos(idAndamento: number) {
    this.CAD232Service.get(idAndamento).then(res => {
      this.CAD232AndamentoList = res;
      this.loading.FecharCarregando();
    });
  }

}
