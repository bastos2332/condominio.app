import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OcorrenciaFormPage } from './ocorrencia-form.page';
import { ComponentModule } from 'src/app/componentes/component/component.module';
import { Storage } from '@ionic/storage';

const routes: Routes = [
  {
    path: '',
    component: OcorrenciaFormPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentModule
  ],
  declarations: [OcorrenciaFormPage],
  providers: [Storage]
})
export class OcorrenciaFormPageModule {}
