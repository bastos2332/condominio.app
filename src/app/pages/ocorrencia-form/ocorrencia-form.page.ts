import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ICAD231SolitacaoOcorrencia } from 'src/app/shared/CAD231/ICAD231SolicitacaoOcorrencia';
import { IUsuario } from 'src/app/shared/Usuario/iusuario';
import { CAD231SolicitacaoOcorrenciaService } from 'src/app/shared/CAD231/Cad231SolitacaoOcorrencia.service';

@Component({
  selector: 'app-ocorrencia-form',
  templateUrl: './ocorrencia-form.page.html',
  styleUrls: ['./ocorrencia-form.page.scss'],
})
export class OcorrenciaFormPage implements OnInit, OnDestroy {
  private sub: any;
  public CAD231: ICAD231SolitacaoOcorrencia;
  public UsuarioLogado: IUsuario;
  constructor(private route: ActivatedRoute, private storage: Storage, private cad231Service: CAD231SolicitacaoOcorrenciaService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const idOcorrencia: number = params['id'];
      if (idOcorrencia) {
        this.CarregarOcorrencia(idOcorrencia);
        console.log(idOcorrencia);
      }
    });
  }

  CarregarOcorrencia(idOCorrencia: number) {
    this.cad231Service.get(idOCorrencia).then(res => {
      this.CAD231 = res;
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
