import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SuportePage } from './suporte.page';
import { ComponentModule } from '../../componentes/component/component.module';

const routes: Routes = [
  {
    path: '',
    component: SuportePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentModule
  ],
  declarations: [SuportePage]
})
export class SuportePageModule {}
