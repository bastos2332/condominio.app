import { Component, OnInit } from '@angular/core';
import { ICAD666Notificacao } from 'src/app/shared/CAD666/icad666-notificacao';
import { CAD666NotificacaoService } from 'src/app/shared/CAD666/cad666-notificacao.service';
import { Router } from '@angular/router';
import { IUsuario } from 'src/app/shared/Usuario/iusuario';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-notificacoes',
  templateUrl: './notificacoes.page.html',
  styleUrls: ['./notificacoes.page.scss'],
})
export class NotificacoesPage implements OnInit {
  tituloHeader = 'Notificações';
  CAD666Notificacoes: ICAD666Notificacao[];
  UsuarioLogado: IUsuario;

  constructor(private cad666NotificacoesService: CAD666NotificacaoService, private route: Router, private storage: Storage) {  }

  ngOnInit() {
    
  }
  ionViewWillEnter(){
    this.CarregarUsuario();
  }

  CarregarUsuario() {
    this.storage.get('usuario').then(res => {
      this.UsuarioLogado = res;
      this.CarregarNotificacoes(this.UsuarioLogado.IdCliente);
    });
  }

  CarregarNotificacoes(idCliente: number) {
    this.cad666NotificacoesService.get(idCliente).then(res => this.CAD666Notificacoes = res);
  }

  GoDestinoNotificacao(caminho: string, idNotificacao: number) {
    this.cad666NotificacoesService.VisualizarConteudoNotificacao(idNotificacao);
    this.route.navigateByUrl(caminho);
  }

  DeleteNotificacao(idNotificacao: number) {
  }

}
