import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FaturasPage } from './faturas.page';
import { MOV101FaturaService } from 'src/app/shared/MOV101/mov101-fatura.service';
import { F006StatusFaturaService } from 'src/app/shared/F006/f006-status-fatura.service';
import { ComponentModule } from 'src/app/componentes/component/component.module';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

const routes: Routes = [
  {
    path: '',
    component: FaturasPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentModule
  ],
  declarations: [FaturasPage],
  providers: [MOV101FaturaService, F006StatusFaturaService, Clipboard, InAppBrowser],
  exports: [FaturasPage]
})
export class FaturasPageModule {}
