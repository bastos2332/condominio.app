import { Storage } from '@ionic/storage';
import { Component, OnInit } from '@angular/core';

import { IMOV101Fatura } from 'src/app/shared/MOV101/imov101-fatura';
import { MOV101FaturaService } from 'src/app/shared/MOV101/mov101-fatura.service';
import { IF006StatusFatura } from 'src/app/shared/F006/if006-status-fatura';
import { F006StatusFaturaService } from 'src/app/shared/F006/f006-status-fatura.service';
import { IUsuario } from 'src/app/shared/Usuario/iusuario';
import { LoadingService } from 'src/app/shared/Loading/loading.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';


@Component({
  selector: 'app-faturas',
  templateUrl: './faturas.page.html',
  styleUrls: ['./faturas.page.scss'],
})
export class FaturasPage implements OnInit {
  public MOV101FaturasList: IMOV101Fatura[];
  public F006StatusFaturaList: IF006StatusFatura[];
  public Usuario: IUsuario;

  constructor
    (
      private clipboard: Clipboard,
      private storage: Storage,
      private theInAppBrowser: InAppBrowser,
      private loading: LoadingService,
      private socialSharing: SocialSharing,
      private mov101FaturaService: MOV101FaturaService,
      private f006StatusFaturaService: F006StatusFaturaService
    ) { }
  
    
    areaTransferencia: string;






    options : InAppBrowserOptions = {
      location : 'yes',//Or 'no' 
      hidden : 'no', //Or  'yes'
      clearcache : 'yes',
      clearsessioncache : 'yes',
      zoom : 'yes',//Android only ,shows browser zoom controls 
      hardwareback : 'yes',
      mediaPlaybackRequiresUserAction : 'no',
      shouldPauseOnSuspend : 'no', //Android only 
      closebuttoncaption : 'Close', //iOS only
      disallowoverscroll : 'no', //iOS only 
      toolbar : 'yes', //iOS only 
      enableViewportScale : 'no', //iOS only 
      allowInlineMediaPlayback : 'no',//iOS only 
      presentationstyle : 'pagesheet',//iOS only 
      fullscreen : 'yes',//Windows only    
  };

  
  public openWithSystemBrowser(url : string){
      let target = "_system";
      this.theInAppBrowser.create(url,target,this.options);
  }
  public openWithInAppBrowser(url : string){
      let target = "_blank";
      this.theInAppBrowser.create(url,target,this.options);
  }
  public openWithCordovaBrowser(url : string){
      let target = "_self";
      this.theInAppBrowser.create(url,target,this.options);
  }  








  ngOnInit() {
    this.loading.Carregando();
    this.CarregarUsuario();

  }

  CarregarUsuario() {
    this.storage.get('usuario').then((res) => {
      this.Usuario = res;
      this.CarregarStatusFatura();
    });
  }

  CarregarStatusFatura() {
    this.f006StatusFaturaService.Get().then(res => {
      this.F006StatusFaturaList = res;
      this.CarregarFaturas();
    });
  }

  CarregarFaturas() {
    this.mov101FaturaService.GetFaturas(this.Usuario.IdProvedor, this.Usuario.IdCliente, false).then(res => {
      this.MOV101FaturasList = res;
      this.loading.FecharCarregando();
    });
  }

  GetStatusFaturaAtual(idStatusFatura: number) {
    for (let i = 0; i < this.F006StatusFaturaList.length; i++) {
      if (this.F006StatusFaturaList[i].IdStatusFatura === idStatusFatura) {
        return this.F006StatusFaturaList[i].NomeStatusFatura;
      }
    }
  }

  CopiarAreaTransferencia() {
    this.clipboard.copy('testando teste');
    console.log('Copiou');
  }

  AbrirFatura(chaveFatura: string, idCliente: number) {
    const target = '_blank';
    const url = 'http://177.101.149.101/CONDOMINIO/UI/BOLETO/form_BOLETO_IMPRIMIR_WEB.aspx?tipoImpressao=IND&chaveBoleto='
    + chaveFatura + '&cliente=' + idCliente + '&tipo=BOL';
    this.theInAppBrowser.create(url, target, this.options);
  }

  async shareWhatsApp() {
    // // Text + Image or URL works
    // this.socialSharing.shareViaWhatsApp(this.text, null, this.url).then(() => {
    //  alert('Compartilhou');
    // }).catch((e) => {
    //   alert('Err ' + e);
    // });
  }


}
