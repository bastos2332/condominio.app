import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NotasVersaoPage } from './notas-versao.page';
import { ComponentModule } from '../../componentes/component/component.module';

const routes: Routes = [
  {
    path: '',
    component: NotasVersaoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentModule
  ],
  declarations: [NotasVersaoPage]
})
export class NotasVersaoPageModule {}
