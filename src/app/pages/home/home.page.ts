import { Component, OnInit } from '@angular/core';

import { CAD229PropagandaService } from 'src/app/shared/CAD229/cad229-propaganda.service';
import { ICAD229Propaganda } from 'src/app/shared/CAD229/ICAD229-propaganda';
import { LoadingService } from 'src/app/shared/Loading/loading.service';
import { IUsuario } from 'src/app/shared/Usuario/iusuario';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  public UsuarioLogado: IUsuario;
  public CAD229List: ICAD229Propaganda[];
  tituloHeader: string;
  constructor(private CAD229Service: CAD229PropagandaService, private loading: LoadingService, private storage: Storage) {
  }


  ngOnInit(): void {
    this.loading.Carregando();
    this.CarregarUsuario();
  }

  CarregarUsuario() {
    this.storage.get('usuario').then(res => {
      this.UsuarioLogado = res;
      this.tituloHeader ='Olá, ' + (this.UsuarioLogado.NomeUsuario).split(" ")[0] + "!";
      this.CarregarPropagandas();
    });
  }

  CarregarPropagandas() {
    this.CAD229Service.getList(this.UsuarioLogado.IdProvedor).then(res => {
      this.CAD229List = res;
      this.loading.FecharCarregando();
    });
  }

}
