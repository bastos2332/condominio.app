import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { CAD229PropagandaService } from 'src/app/shared/CAD229/cad229-propaganda.service';
import { ComponentModule } from 'src/app/componentes/component/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    RouterModule.forChild([{ path: '', component: HomePage }])
  ],
  declarations: [HomePage],
  providers: [CAD229PropagandaService],
  exports: [HomePage]
})
export class HomePageModule {}
