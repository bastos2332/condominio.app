import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MeuCondominioPage } from './meu-condominio.page';
import { ComponentModule } from 'src/app/componentes/component/component.module';
import { OcorrenciasPageModule } from '../ocorrencias/ocorrencias.module';
import { AgendaPageModule } from '../agenda/agenda.module';
import { FaturasPageModule } from '../faturas/faturas.module';

const routes: Routes = [
  {
    path: '',
    component: MeuCondominioPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentModule,
    OcorrenciasPageModule,
    AgendaPageModule,
    FaturasPageModule

  ],
  declarations: [MeuCondominioPage]
})
export class MeuCondominioPageModule {}
