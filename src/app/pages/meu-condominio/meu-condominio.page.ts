import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-meu-condominio',
  templateUrl: './meu-condominio.page.html',
  styleUrls: ['./meu-condominio.page.scss'],
})
export class MeuCondominioPage implements OnInit {

  isAgenda: boolean = false;
  isOcorrencia: boolean = true;
  isFatura: boolean = false;
  tituloHeader: string = 'Meu condomínio'
  constructor() { }

  ngOnInit() {
  }
  segmentButtonClicked(ev: CustomEvent) {
     switch ((<HTMLInputElement>(ev.target)).value) {
      case 'ocorrencias':
        this.isOcorrencia = true;
        this.isAgenda = false;
        this.isFatura = false;
        break;
      case 'agenda':
        this.isOcorrencia = false;
        this.isAgenda = true;
        this.isFatura = false;
        break;
      case 'faturas':
        this.isOcorrencia = false;
        this.isAgenda = false;
        this.isFatura = true;
        break;
    }
  }

}
