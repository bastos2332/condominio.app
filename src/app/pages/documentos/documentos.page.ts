import { Component, OnInit } from '@angular/core';
import { IUsuario } from 'src/app/shared/Usuario/iusuario';
import { CAD228ArquivoService } from 'src/app/shared/CAD228/cad228-arquivo.service';
import { Storage } from '@ionic/storage';
import { LoadingService } from 'src/app/shared/Loading/loading.service';
import { ICAD228Arquivo } from 'src/app/shared/CAD228/icad228-arquivo';

@Component({
  selector: 'app-documentos',
  templateUrl: './documentos.page.html',
  styleUrls: ['./documentos.page.scss'],
})
export class DocumentosPage implements OnInit {
  tituloHeader: string = "Documentos do condomínio"
  public UsuarioLogado: IUsuario;
  public CAD228ArquivoList: ICAD228Arquivo[];

  constructor(private cad228Service: CAD228ArquivoService, private storage: Storage, private loading: LoadingService) { }

  ngOnInit() {
    this.loading.Carregando();
    this.CarregarUsuario();
  }

  CarregarUsuario() {
    this.storage.get('usuario').then(res => {
      this.UsuarioLogado = res;
      this.CarregarDocumentos();
    });
  }

  CarregarDocumentos() {
    this.cad228Service.get(this.UsuarioLogado.IdProvedor).then(res => {
      this.CAD228ArquivoList = res;
      this.loading.FecharCarregando();
    });
  }

  OpenDocumento() {
    window.open('http://177.101.149.101/condominio/UPLOAD/IMAGENS_CLIENTE/CLIENTE_32707/Conve%C3%A7%C3%A3o.pdf', '_system', 'location=yes')
  }

}
