import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DocumentosPage } from './documentos.page';
import { ComponentModule } from 'src/app/componentes/component/component.module';

const routes: Routes = [
  {
    path: '',
    component: DocumentosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentModule
  ],
  declarations: [DocumentosPage]
})
export class DocumentosPageModule {}
