import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/shared/Usuario/usuario.service';
import { IUsuarioAuth } from 'src/app/shared/Usuario/iusuario-auth';
import { Storage } from '@ionic/storage';
import swal from 'sweetalert';
import { LoadingService } from 'src/app/shared/Loading/loading.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public Auth: IUsuarioAuth;
  constructor
    (
      private route: Router,
      private UsuairoService: UsuarioService,
      private storage: Storage,
      private loading: LoadingService
    ) { }

  login: string;
  senha: string;

  ngOnInit() {
  }

  Logar() {
    this.loading.Carregando();
    this.Auth = {
      login: this.login,
      senha: this.senha
    };

    this.UsuairoService.Logar(this.Auth).then(usuario => {
      if (usuario) {
        this.storage.set('usuario', usuario);
        this.route.navigateByUrl('/tabs');
      } else {
        swal('Erro', 'Usuário ou senha inválido!', 'error');
      }

      this.loading.FecharCarregando();
    });

    this.LimparCampos();
  }

  LimparCampos() {
    this.login = '';
    this.senha = '';
  }

}
