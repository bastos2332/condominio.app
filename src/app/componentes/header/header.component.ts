import { Component, OnInit, Input } from '@angular/core';
import { IUsuario } from 'src/app/shared/Usuario/iusuario';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  public UsuarioLogado: IUsuario;
  @Input() titulo: string;
  constructor(private storage: Storage) { }

  ngOnInit() {
    this.CarregarUsuario();
  }

  CarregarUsuario() {
    this.storage.get('usuario').then(res => {
      this.UsuarioLogado = res;
    });
  }

}
