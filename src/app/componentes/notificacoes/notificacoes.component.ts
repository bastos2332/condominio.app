import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { ICAD666Notificacao } from 'src/app/shared/CAD666/icad666-notificacao';
import { CAD666NotificacaoService } from 'src/app/shared/CAD666/cad666-notificacao.service';
import { IUsuario } from 'src/app/shared/Usuario/iusuario';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-notificacoes',
  templateUrl: './notificacoes.component.html',
  styleUrls: ['./notificacoes.component.scss'],
})
export class NotificacoesComponent implements OnInit {

  public UsuarioLogado: IUsuario;
  private QuantidadeNotificacoes: number = 0;

  constructor(private route: Router, private cad666NotificacoesService: CAD666NotificacaoService, private storage: Storage) {

   }

  

  ngOnInit() {
    this.CarregarUsuario();
  }

  CarregarUsuario() {
    this.storage.get('usuario').then(res => {
      this.UsuarioLogado = res;
      this.CarregarNotificacoesNaoVisualizadas();
    });
  }

  CarregarNotificacoesNaoVisualizadas() {
    const escopo = this;
    this.cad666NotificacoesService.get(this.UsuarioLogado.IdCliente).then(function(res) {
      res.map(function(elemento) {
        if (! elemento.IsVisualizada) {
          escopo.QuantidadeNotificacoes++;
        }
      });
    });
  }

  VisualizarNotificacoes() {
      this.cad666NotificacoesService.VisualizarAlertaNotificacao(this.UsuarioLogado.IdCliente);
    this.route.navigateByUrl('/tabs/notificacoes');
    this.QuantidadeNotificacoes = 0;
  }
}
