import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CAD666NotificacaoService } from 'src/app/shared/CAD666/cad666-notificacao.service';

import { HeaderComponent } from '../header/header.component';
import { NotificacoesComponent } from '../notificacoes/notificacoes.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [HeaderComponent, NotificacoesComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule

  ],
  exports: [HeaderComponent, NotificacoesComponent],
  providers: [CAD666NotificacaoService]
})
export class ComponentModule { }
