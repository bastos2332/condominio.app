import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';
import { IUsuario } from './shared/Usuario/iusuario';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styles: ['ion-label, ion-text, ion-title, ion-item h1, h2, h3, h4, h5, a{font-family: "Quicksand", sans-serif!important;}']
})
export class AppComponent implements OnInit {
  public usuarioLogado: IUsuario;
  pages = [
    {
      title: 'Ajustes',
      childrens: [
        {
          title: 'Perfil',
          url: '/tabs/perfil',
          urlIcon: '../assets/icon/ico-perfil.svg'
        }
      ]
    },
    {
      title: 'Ajuda',
      childrens: [
        {
          title: 'Suporte',
          url: '/tabs/suporte',
          urlIcon: '../assets/icon/ico-suporte2.svg'
        },
        {
          title: 'Sobre',
          url: '/tabs/sobre',
          urlIcon: '../assets/icon/ico-sobre2.svg'
        },
        {
          title: 'Notas da Versão - 0.0.1',
          url: '/tabs/notas-versao',
          urlIcon: '../assets/icon/ico-versao2.svg'
        }
      ]
    },
    {
      title: '#',
      childrens: [
        {
          title: 'Sair',
          url: '/login',
          click: function() {
            this.storage.set('usuario', '');

          }.bind(this),
        urlIcon: '../assets/icon/ico-sair2.svg'}
      ]
    }
  ];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage,
    private route: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

goPerfil(){
  this.route.navigateByUrl('/tabs/perfil')
}

  ngOnInit() {
    this.storage.get('usuario').then((val) => {
      this.usuarioLogado = val;
    });
  }
}
