import { SuportePageModule } from '../pages/suporte/suporte.module';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs.router.module';
import { TabsPage } from './tabs.page';
import { MeuCondominioPageModule } from '../pages/meu-condominio/meu-condominio.module';
import { NotificacoesPageModule } from '../pages/notificacoes/notificacoes.module';
import { PerfilPageModule } from '../pages/perfil/perfil.module';
import { SobrePageModule } from '../pages/sobre/sobre.module';
import { NotasVersaoPageModule } from '../pages/notas-versao/notas-versao.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule,
    MeuCondominioPageModule,
    NotificacoesPageModule,
    PerfilPageModule,
    SobrePageModule,
    SuportePageModule,
    NotasVersaoPageModule
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {
}
