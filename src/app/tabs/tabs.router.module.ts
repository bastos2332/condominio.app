import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { MeuCondominioPage } from '../pages/meu-condominio/meu-condominio.page';
import { NotificacoesPage } from '../pages/notificacoes/notificacoes.page';
import { PerfilPage } from '../pages/perfil/perfil.page';
import { SuportePage } from '../pages/suporte/suporte.page';
import { SobrePage } from '../pages/sobre/sobre.page';
import { NotasVersaoPage } from '../pages/notas-versao/notas-versao.page';
import { FaturasPage } from '../pages/faturas/faturas.page';
import { OcorrenciasPage } from '../pages/ocorrencias/ocorrencias.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'documentos',
        children: [
          {
            path: '',
            loadChildren: '../pages/documentos/documentos.module#DocumentosPageModule'
          }
        ]
      },
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: '../pages/home/home.module#HomePageModule'
          }
        ]
      },
      {
        path: 'faturas',
        children: [
          {
            path: '',
            component: FaturasPage
          }
        ]
      },
      {
        path: 'meu-condominio',
        children: [
          {
            path: '',
            component: MeuCondominioPage
          }
        ]
      },
      {
        path: 'notificacoes',
        children: [
          {
            path: '',
            component: NotificacoesPage
          }
        ]
      },
      {
        path: 'perfil',
        children: [
          {
            path: '',
            component: PerfilPage
          }
        ]
      },
      {
        path: 'suporte',
        children: [
          {
            path: '',
            component: SuportePage
          }
        ]
      },
      {
        path: 'sobre',
        children: [
          {
            path: '',
            component: SobrePage
          }
        ]
      },
      {
        path: 'notas-versao',
        children: [
          {
            path: '',
            component: NotasVersaoPage
          }
        ]
      },
      {
        path: 'ocorrencias',
        children: [
          {
            path: '',
            loadChildren: '../pages/ocorrencias/ocorrencias.module#OcorrenciasPageModule'
          },
          {
            path: 'form/:id',
            loadChildren: '../pages/ocorrencia-form/ocorrencia-form.module#OcorrenciaFormPageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
